#include "fname.inc"

// System V amd64:  rdi, rsi, rdx, rcx, r8, r9 / xmm0−xmm7  + al


.globl FNAME(init1)
.globl FNAME(init4)
.globl FNAME(initt)
.globl FNAME(block_init_if_aligned)

// ****************************************************************************
// extern "C" void init1(char *M, size_t M_len);

.func
FNAME(init1):     
    // System V amd64: указатель M в %rdi, длина M_len в %rsi (size_t размера указателя)
    
    sub $8, %rsp
    
    mov $'a', %al
    xorl %ecx, %ecx
begin_iteration_1:
    cmp %rsi, %rcx
    jge end_loop_1
    mov %al, (%rdi, %rcx)
    inc %al
    inc %rcx
    jmp begin_iteration_1
end_loop_1:
   
    add $8, %rsp
    ret 
    // выравнивать/восстанавливать %rsp здесь не обязательно, так как нет ни вложенных вызовов, ни xmm-команд — но пусть будет, чтобы плохому не учились
.endfunc

// ****************************************************************************
// extern "C" void init4(int *M, size_t M_len);

.func
FNAME(init4): 
    sub $8, %rsp
    
    xorl %ecx, %ecx
begin_iteration_4:
    cmp %rsi, %rcx
    jge end_loop_4
    movl $0, (%rdi, %rcx, 4)
    inc %rcx
    jmp begin_iteration_4
end_loop_4:
   
    add $8, %rsp
    ret 
    // выравнивать/восстанавливать %rsp здесь не обязательно — но пусть будет
.endfunc

// ****************************************************************************
// extern "C" void initt(long double *M, size_t M_len, size_t long_double_size);

.func
FNAME(initt): 
    sub $8, %rsp
    
begin_iteration_T:
    fldpi
    fstpt (%rdi)
    add %rdx, %rdi
    dec %rsi
    jnz begin_iteration_T
   
    add $8, %rsp
    ret 
    // выравнивать/восстанавливать %rsp здесь не обязательно — но пусть будет
.endfunc


// ****************************************************************************
// extern "C" int block_init_if_aligned(double *M, size_t M_len_div_4);

.func
FNAME(block_init_if_aligned): 
    test $(32-1), %rdi // должно быть 16 штук нулей; маскируем 16-ю единицами
    jnz bad_addr_ymm    

    pushq $0 // 1) выравнивание %rsp; 2) значение для инициализации  +0 double = все нули = 0 quad
    vbroadcastsd (%rsp), %ymm0
    
begin_iteration_ymm:
    // обнуление 4×double командами общего назначения (допускается и при bad_addr_ymm, но для иллюстрации не делаем) 
    //movq $0, (%rdi)
    //movq $0, 8(%rdi)
    //movq $0, 16(%rdi)
    //movq $0, 24(%rdi)
    // обнуление 4×double как невыравненных (допускается и при bad_addr_ymm, но для иллюстрации не делаем)
    //vmovupd %ymm0, (%rdi) // vmovApd — di должен быть выравнен на 16
    // обнуление 4×double как выравненных
    vmovapd %ymm0, (%rdi) // vmovApd — di должен быть выравнен на 16
    add $(8*4), %rdi
    dec %rsi
    jnz begin_iteration_ymm

good_ret:    
    xor %eax, %eax
    add $8, %rsp
    ret 
    // а вот здесь выравнивать/восстанавливать %rsp обязательно
    
bad_addr_ymm:
    mov $-1, %eax
    ret 
  
.endfunc


