#include <iostream>

int foo(int x)
{
    return 3*x + 1;
}

int main()
{
    int x = 13;
    std::cout << "foo(" << x << ") = " << foo(x) << std::endl;
    return 0;
}
