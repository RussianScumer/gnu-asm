// Макрос для компенсации искажения имён в Mac OS X (и в MS Windows 32, но в этой программе неактуально)
#ifdef __APPLE__
#define FNAME(s) _##s
#elif _WIN64
#define FNAME(s) s
#elif _WIN32
#define FNAME(s) _##s
#else
#define FNAME(s) s
#endif 

.data 
    fmt: .string "%d, %d, %d, %d, %d, %d\n" // 6 чисел → 7 параметров printf()
   
.text
.globl FNAME(main)

FNAME(main):        
    // − 8⋅(2x+1) для выравнивания стека 
    // (2x+1) ≥ (стек.перем) + max(стек.парам.)
    // (2x+1) ≥ 0+1 = 1
    sub $8, %rsp 
        
    // SysV amd64: di, si, D, C, r8, r9, стек + al
    lea fmt(%rip), %rdi
    mov $2, %esi
    mov $3, %edx
    mov $4, %ecx
    mov $5, %r8d
    mov $6, %r9d
    movq $7, (%rsp)
    mov $0, %al   
    call FNAME(printf)

    add $8, %rsp 
    
    xor %eax, %eax 
    ret 
