#include <iostream>
#include <iomanip>
#include <cmath>

int main()
{
    // typedef double T;
    // const int tdecdigits = 15; 
    
    typedef float T;
    const int tdecdigits = 10; // точность float порядка 7 знаков — так что это с большим запасом

    const double s = 1.05;
    const double exp = -s;
    
    std::cout.setf(std::ios::fixed);    
    std::cout.precision(tdecdigits-2); // ...две из значащих цифр до запятой
    
    std::cout << "lim S = ζ(s) = " << std::riemann_zeta(s) << std::endl; // -std=c++17
    
    std::cout << std::setw(4) << "N" << " " << std::setw(tdecdigits+2) << "S1" << " " << std::setw(tdecdigits+2) << "S2" << " " << std::setw(tdecdigits+2) << "a_N" << std::endl;
    
    for(int nn = 0; nn < 10; ++nn)
    {
        long long N = pow(10, nn);
        T S1=0, S2=0, ai;

        for(long long i = 1; i <= N; ++i)
        {
            ai = pow(i, exp);
            S1 += ai;
        }

        for(long long i = N; i >=1; --i)
        {
            ai = pow(i, exp);
            S2 += ai;
        }
        
        ai = pow(N, exp);

        std::cout << "10^" << nn << " " << std::setw(tdecdigits+2) << S1 << " " << std::setw(tdecdigits+2) << S2 << " " << std::setw(tdecdigits+2) << ai << std::endl;
    } 
       
    return 0;
}
