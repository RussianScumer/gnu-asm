// Макрос для компенсации искажения имён в Mac OS X (и в MS Windows 32, но в этой программе неактуально)
#ifdef __APPLE__
#define FNAME(s) _##s
#elif _WIN64
#define FNAME(s) s
#elif _WIN32
#define FNAME(s) _##s
#else
#define FNAME(s) s
#endif 
