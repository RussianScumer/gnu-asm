#include <iostream>
#include <iomanip>

template <typename T> void print(T *M, size_t M_len)
{
    using namespace std;
    
    for(size_t i = 0; i < M_len; ++i)
         cout << M[i] << " ";
     cout << endl;
    
    //for(size_t i = 0; i < M_len; ++i)
    //    cout << hex << *(reinterpret_cast<unsigned long long *>(&M[i])) << " ";
    //cout << endl;
}


extern "C" void init1(char *M, size_t M_len);
extern "C" void init4(int *M, size_t M_len);
extern "C" void initt(long double *M, size_t M_len, size_t long_double_size); // long_double_size может быть и 12, и 16
extern "C" int block_init_if_aligned(double *M, size_t M_len_div_4); // возвращает 0 при успешной инициализации, -1 при неуспехе

int main()
{
    const int N = 8;
    int i, x;
    char M1[N];
    asm
    (
    "movb $'a', %b[X]\n"
    "xorl %[I], %[I]\n"
    "begin_iteration:\n"
    "cmpl %[M_len], %[I]\n"
    "jge end_loop\n"
    "movb %b[X], (%[M],%q[I])\n"
    "incb %b[X]\n"
    "incl %[I]\n"
    "jmp begin_iteration\n"
    "end_loop:\n"
    :[I]"=&r"(i), [X]"=&q"(x)
    :[M_len]"i"(N), [M]"r"(M1)
    : "cc","memory"
    );
    print(M1, N);
    
    init1(M1, N);
    print(M1, N);
   
    int M4[N];
    init4(M4, N);
    print(M4, N);
    
    long double Mt[N];
    initt(Mt, N, sizeof(long double));
    print(Mt, N);
    
    double Md[N];
    std::cout << std::endl;
    std::cout << "addr: " << Md;
    std::cout << ",  init result: " << block_init_if_aligned(Md, 2) << ",  values: "  << std::endl;
    print(Md, N);
    
    

    return 0;
}



