#include <iostream>
#include <iomanip>

typedef struct 
{
    char Tag;
    char align[3];
    int  Val;
} 
TSomeStruct;

extern "C" void init(TSomeStruct *p);


int main()
{
    TSomeStruct s;
    asm
    (
    "movb $'a', (%[S])\n"
    "movl $13, 4(%[S])\n"
    :
    :[S]"r"(&s)
    : "memory"
    );

    std::cout << s.Tag << " " << s.Val << std::endl;

    init(&s);
    std::cout << s.Tag << " " << s.Val << std::endl;
       
    return 0;
}



