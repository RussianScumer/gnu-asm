# noexec на флешке #!/bin/bash

MAINFILE=''
SRCDIRNAME=''
BASEFILENAME=''

SRCROOTDIR='tex-src'

TARGETTYPE=''

RUNSCOUNT='1'
HELPLINE="Запуск: bash ${0} [ b|book|p N|pres N|l|buglist -1|-2|-f|--full | -h]"

numargs="$#"
for ((i=1 ; i <= numargs ; i++)); do

  arg="${1}"
  shift
  
  case "$arg" in
  -1)    
    RUNSCOUNT='1'
    ;;
  -2)    
    RUNSCOUNT='2'
    ;;     
  -f|--full)    
    RUNSCOUNT='f'
    ;; 
#   --bib)    
#     RUNSCOUNT='bib'
#     ;;    
  b|book)    
    SRCDIRNAME="book"
    BASEFILENAME=0main
    ;;     
  l|buglist)    
    SRCDIRNAME="book"
    BASEFILENAME=0bugs
    ;;     
  p|pres)    
    SRCDIRNAME="pres"
    FILENAMEWITHPATH=`ls ${SRCROOTDIR}/${SRCDIRNAME}/${1}*`
    TEXFILENAME=`basename ${FILENAMEWITHPATH}`
    BASEFILENAME="${TEXFILENAME/.tex/}"
    echo $BASEFILENAME
    shift
    ;;
  d)    
    MAINFILE="${1}"
    shift
    
    TEXFILENAME=`basename ${MAINFILE}`
    BASEFILENAME="${TEXFILENAME/.tex/}"
    RESFILE=${MAINFILE/.tex/.pdf}
    ;;
        
  -h)
    echo "${HELPLINE}"
    ;;    
  esac  
done



if [[ (-z "${BASEFILENAME}") ]]
then
  echo "Не задано имя файла."
  echo "${HELPLINE}"
  exit
fi


if ! [ -d tmp/ ]; then
  mkdir tmp/
fi


if [[ (-z "${MAINFILE}") ]]
then

    MAINFILE=${SRCROOTDIR}/${SRCDIRNAME}/${BASEFILENAME}
fi    

COMPILEDFILE="tmp/${BASEFILENAME}.pdf"

# COMPRESSEDFILE="${COMPILEDFILE}.pdf"
MINSIZEFILE=${COMPILEDFILE}

# RESFILE="kai-rndnet-${BASEFILENAME}.pdf"




MKTXT="pdflatex --output-directory=tmp ${MAINFILE}"
MKBIB="bibtex tmp/${BASEFILENAME}"

echo "${MKTXT}"

# pdflatex ${BASEFILENAME}

case "$SRCDIRNAME" in
  book)
  RESFILE=gnu-asm-theory-labs.pdf
  
if [[ "${BASEFILENAME}" == "0bugs" ]]; then
  RESFILE=Замеченные\ опечатки.pdf
fi  
  

  case "$RUNSCOUNT" in
    1)    
      ${MKTXT}
    ;;     
    2)    
      ${MKTXT} &&  ${MKTXT}
    ;;     
#     bib)    
# #         ${MKTXT} && ${MKTXT} && ${MKBIB} && ${MKBIB} && ${MKTXT}&& ${MKTXT}
# 	rm "tmp/${BASEFILENAME}.bbl" "tmp/${BASEFILENAME}.blg"
#         ${MKBIB} && ${MKBIB} && ${MKTXT} # удалять только bib
#     ;;      
    f)        
#       ${MKTXT} &&  ${MKTXT} && ${MKBIB} && ${MKBIB} && ${MKTXT} &&  ${MKTXT}
      if ${MKTXT} 
      then
      
        ${MKTXT} && ${MKBIB} && ${MKBIB} && ${MKTXT}
        
        cd tmp/
        # utf-8 не годится, нужна однобайтовая кодировка (любая подходит, но официально результат rumakeindex в koi8)
        LANG=ru_RU.KOI8-RU rumakeindex ${BASEFILENAME}.idx
        iconv -f KOI8-RU -t WINDOWS-1251 ${BASEFILENAME}.ind -o ${BASEFILENAME}.ind
        cd ..
        
        ${MKTXT} && ${MKTXT}
  
  
        # ps2pdf -dUseFlatCompression=true ${COMPILEDFILE}
      else
	exit
      fi
    ;;    
  esac  
  
  ;;
  

  
  pres)
  RESFILE=${BASEFILENAME}.pdf
  case "$RUNSCOUNT" in
    1)    
      ${MKTXT}
    ;;     
    2|f)    
      ${MKTXT} &&  ${MKTXT}
    ;;    
  esac  
  ;;    
  
   '')
  RESFILE=${BASEFILENAME}.pdf
  case "$RUNSCOUNT" in
    1)    
      ${MKTXT}
    ;;     
    2|f)    
      ${MKTXT} &&  ${MKTXT}
    ;;    
  esac  
  ;;   

  
esac




echo "Копирую ${MINSIZEFILE} в ${RESFILE}..."
cp "${MINSIZEFILE}" "${RESFILE}"

# echo "Копирую в корень флешки..."
# cp ${MINSIZEFILE} "../../${RESFILE}"

# echo "Копирую в github-stub..."
# cp ${MINSIZEFILE} "../github-stub/${RESFILE}"

